const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ImageUpload = new Schema({
  title: String,
  path: String,
  width: Number,
  height: Number
});


mongoose.model('imageuploads', ImageUpload);

mongoose.connect('mongodb://localhost/node-imageuploads');
