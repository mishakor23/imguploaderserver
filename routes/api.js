const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const multer = require('multer');
const sizeOf = require('image-size');
const ImageUpload = mongoose.model('imageuploads');
const lwip = require('lwip');
const fs = require('fs');
const fileExtension = require('file-extension');

const storage = multer.diskStorage({
  destination: (req, file, cd) => {
    cd(null, './uploads');
  },
  filename: (req, file, cd) => {
    const ext = fileExtension(file.originalname);
    cd(null, `${file.fieldname}-${Date.now()}.${ext}`);
  }
});
const upload = multer({ storage });


router.get('/images', (req, res, next) => {
  ImageUpload.find((err, imageuploads) => {
    if (err) {
      next(err);
      return false;
    }
    res.send({ data: imageuploads });
    return false;
  });
});


router.get('/images/:id', (req, res) => {
  const query = { _id: req.params.id };
  ImageUpload.findOne(query, (err, imageupload) => {
    // console.log(imageupload);
    res.send(imageupload);
  });
});

router.post('/images', upload.single('image'), (req, res) => {
  const dimensions = sizeOf(req.file.path);
  new ImageUpload({
    title: req.body.title,
    path: req.file.path,
    width: dimensions.width,
    height: dimensions.height })
  .save((err, imageupload) => {
    res.send({
      title: req.file.filename,
      path: req.file.path,
      width: dimensions.width,
      height: dimensions.height,
      id: imageupload.id
    });
  });
});

router.post('/:id/rotate', (req, res) => {
  const query = { _id: req.params.id };
  ImageUpload.findOne(query, (err, imageupload) => {
    lwip.open(imageupload.path, (err, image) => {
      image.rotate(90, (err, rtImage) => {
        rtImage.writeFile(imageupload.path, () => {
          res.send({ path: imageupload.path });
        });
      });
    })
  });
});

router.post('/:id/scale', (req, res) => {
  const query = { _id: req.params.id };
  ImageUpload.findOne(query, (err, imageupload) => {
    lwip.open(imageupload.path, (err, image) => {
      image.scale(+req.body.size, (err, rtImage) => {
        rtImage.writeFile(imageupload.path, () => {
          res.send({ path: imageupload.path });
        });
      });
    });
  });
});


router.delete('/:id/delete/', (req, res) => {
  const query = { _id: req.params.id };
  ImageUpload.findOneAndRemove(query, (err, imageupload) => {
    fs.unlink(imageupload.path, (err) => {
      if (err) {
        return console.error(err);
      }
    });
    res.send('deleted!');
  });
});

module.exports = router;
